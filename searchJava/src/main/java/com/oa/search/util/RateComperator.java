package com.oa.search.util;

import com.oa.search.entity.Product;

import java.util.Comparator;
import java.util.Map;

public class RateComperator implements Comparator<Map.Entry<Product, Double>> {
	
	@Override
	public int compare(Map.Entry<Product, Double> o1, Map.Entry<Product, Double> o2) {
		return o2.getValue().compareTo(o1.getValue());
	}
}
