package com.oa.search.util;

import com.oa.search.entity.Category;
import com.oa.search.entity.Product;
import com.oa.search.entity.WordCorrection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class SearchKeywords {
	List<Category> categoryList = new ArrayList<>();
	List<WordCorrection> wordCorrectionList = new LinkedList<>();
	
	public List<Product> prepareList(List<Product> productList, String searchText){
		initData();
		double rank, oldVal;
		List<Product> sortedProductList = new LinkedList<>();
		Map<Product, Double> finalProductSortingMap = new HashMap<>();
		
		searchText = corrertWords(searchText);
		
		String[] keywords = searchText.split(" ");
		
		//------------ keyword INDEX in title -------------------------
		for (Product product : productList) {
			if (product.getTitle().toLowerCase().contains(searchText.toLowerCase())) {
				//apply index rank
				rank = product.getTitle().toLowerCase().indexOf(searchText.toLowerCase());
				finalProductSortingMap.put(product, rank);
			}
		}
		List<Map.Entry<Product,Double>> entries = new LinkedList<>( finalProductSortingMap.entrySet());
		
		// first, sort by index rank (min to max)
		Collections.sort(entries, new IndexComperator());
		
		double addRank = entries.size();
		for (Map.Entry<Product, Double> entry : entries) {
			// reverse ranks (max will shown at top)
			finalProductSortingMap.replace(entry.getKey(), addRank--);
		}
		
		
		//----------------- keyword / title rank + category rank -------------------------
		
		for (Product product : productList) {
			if (product.getTitle().toLowerCase().contains(searchText.toLowerCase())) {

				// apply keyword / title rank (increase as rank)
				rank = ((double)searchText.length() / product.getTitle().length());
				
				oldVal = finalProductSortingMap.replace(product, 0d);
				finalProductSortingMap.replace(product, oldVal * ( rank + 1));
				
				//System.out.println( rank + " | " + product.getId() + " - " + product.getTitle());
				
				// apply category rank (increase as rank)
				rank = (categoryList.stream().filter(c-> c.getId() == product.getCategoryId()).findFirst().get().getSortRate());
				
				oldVal = finalProductSortingMap.replace(product, 0d);
				finalProductSortingMap.replace(product, oldVal * ( rank + 1));
			}else if (keywords.length > 1){
				finalProductSortingMap.put(product, 0.1); // give default value as initial
				for (int i = 0; i < keywords.length; i++) {
					if ((product.getTitle().toLowerCase()).contains(keywords[i].toLowerCase())) {
						oldVal = finalProductSortingMap.replace(product,0d);
						finalProductSortingMap.replace(product, oldVal * ( 1.0 + (double)(keywords.length-i)/keywords.length)); // increase rank by index rate of keyword in searchText
					}
				}
			}
		}
		//------------------------------------------------
		
		entries = new LinkedList<>( finalProductSortingMap.entrySet());
		Collections.sort(entries, new RateComperator());
		
		// printed in detail
		// entries.stream().forEach(entry -> System.out.println(String.format("%.3f", entry.getValue()) + " | " + entry.getKey().getId()+" - "+entry.getKey().getTitle()));
		
		entries.stream().filter(entry -> entry.getValue() > 0.1).forEach(entry ->  sortedProductList.add(entry.getKey()));
		
		return sortedProductList;
	}
	
	private String corrertWords(String searchText) {
		searchText = searchText.trim();
		System.out.println("Searching Text : " + searchText);
		String correctedText = searchText;
		for (WordCorrection wordCorrection : wordCorrectionList) {
			correctedText = correctedText.toLowerCase().replace(wordCorrection.getWrongWord().toLowerCase(), wordCorrection.getCorrectWord().toLowerCase());
		}
		if (!correctedText.toLowerCase().equals(searchText.toLowerCase()))
			System.out.println("Corrected as : " + correctedText);
		
		return correctedText;
	}
	
	private void initData() {
		categoryList.add(new Category(0,"Phone", 0.0));
		categoryList.add(new Category(1,"Accessory", 0.36));
		
		wordCorrectionList.add(new WordCorrection("sayomi", "xaomi"));
		wordCorrectionList.add(new WordCorrection("ayfon", "iphone"));
		
	}
	
}
