package com.oa.search.util;

import com.oa.search.entity.Product;

import java.util.Comparator;
import java.util.Map;

public class IndexComperator implements Comparator<Map.Entry<Product, Double>> {
	
	@Override
	public int compare(Map.Entry<Product, Double> o1, Map.Entry<Product, Double> o2) {
		return o1.getValue().compareTo(o2.getValue());
	}
}
