package com.oa.search.entity;

import lombok.Getter;

@Getter
public class WordCorrection {
	private String wrongWord;
	private String correctWord;
	
	
	public WordCorrection(String wrongWord, String correctWord) {
		this.wrongWord = wrongWord;
		this.correctWord = correctWord;
	}
}


