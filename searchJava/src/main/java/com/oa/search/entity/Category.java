package com.oa.search.entity;

import lombok.Getter;


@Getter
public class Category {
	
	private int id;
	
	private String name;
	private double sortRate;
	
	public Category(int id, String name, double sortRate) {
		this.id = id;
		this.name = name;
		this.sortRate = sortRate;
	}
	
	public Category(int id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public Category() {
	}
	
}
