package com.oa.search.entity;

import lombok.Getter;


@Getter
public class Product {
	
	private int id;
	
	private String title;
	private int categoryId;
	private boolean isActive;
	
	public Product(int id, String title, int categoryId, boolean isActive) {
		this.id = id;
		this.title = title;
		this.categoryId = categoryId;
		this.isActive = isActive;
	}
	
	public Product() {
	}
	
	@Override
	public String toString() {
		return id + " - " + title;
	}
}
