import com.oa.search.entity.Category;
import com.oa.search.entity.Product;
import com.oa.search.util.SearchKeywords;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;


public class SearchServiceTest {
	
	List<Product> productList = new ArrayList<>();
	List<Category> categoryList = new ArrayList<>();
	

	@Test
	public void testSearcService(){
		String searchText = "Ayfon 11";
		
		SearchKeywords searchKeywords = new SearchKeywords();
		
		List<Product> list = searchKeywords.prepareList(productList, searchText);
		
		list.stream().forEach(product -> System.out.println(product.toString()));
	}
	
	@Before
	public void prepareData(){
		categoryList.add(new Category(0,"Phone", 0.0));
		categoryList.add(new Category(1,"Accessory", 0.36));
		
		productList.add(new Product(1,"Spigen Apple IPhone 11 Pro Max Kılıf",categoryList.stream().parallel().filter(c -> c.getName().equals("Phone")).findFirst().get().getId(),true));
		productList.add(new Product(2,"Iphone 11 128 GB Cep telefonu",categoryList.stream().parallel().filter(c -> c.getName().equals("Accessory")).findFirst().get().getId(),true));
		productList.add(new Product(3,"Apple IPhone 11 Pro Max Deri Kılıf",categoryList.stream().parallel().filter(c -> c.getName().equals("Phone")).findFirst().get().getId(),true));
		productList.add(new Product(4,"IPhone 11 Spingen Mavi Pro Max Kılıf",categoryList.stream().parallel().filter(c -> c.getName().equals("Accessory")).findFirst().get().getId(),true));
		productList.add(new Product(5,"IPhone 10 64 GB Cep telefonu",categoryList.stream().parallel().filter(c -> c.getName().equals("Phone")).findFirst().get().getId(),true));
		productList.add(new Product(6,"Iphone 11 Pro Max Silikon Siyah Kılıf",categoryList.stream().parallel().filter(c -> c.getName().equals("Accessory")).findFirst().get().getId(),true));
		productList.add(new Product(7,"Xaomi 11 Silikon Siyah Kılıf",categoryList.stream().parallel().filter(c -> c.getName().equals("Accessory")).findFirst().get().getId(),true));
		productList.add(new Product(8,"IPhone 7 Silikon Siyah Kılıf",categoryList.stream().parallel().filter(c -> c.getName().equals("Accessory")).findFirst().get().getId(),true));
		productList.add(new Product(9,"Sony Xperia 10 Silikon Siyah Kılıf",categoryList.stream().parallel().filter(c -> c.getName().equals("Accessory")).findFirst().get().getId(),true));
		productList.add(new Product(0,"Samsung Note 20 256 GB Yeşil",categoryList.stream().parallel().filter(c -> c.getName().equals("Accessory")).findFirst().get().getId(),true));
	}
}
